import datetime

import pytz
import requests


def get_distance_matrix(origins, destinations, api_key):
    url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
    params = {
        'key': api_key,
        'mode': 'driving',
        'departure_time': 'now',
        'origins': '|'.join(origins),
        'destinations': '|'.join(destinations)
    }

    response = requests.get(url, params=params)

    if response.status_code != 200:
        raise Exception('Wrong status code!')

    return response.json()


def parse_response(response, tz):
    parsed_rows = []
    for i, origin in enumerate(response['origin_addresses']):
        for j, destination in enumerate(response['destination_addresses']):
            row = response['rows'][i]['elements'][j]
            parsed_rows.append({
                'datetime': tz.localize(datetime.datetime.now()).isoformat(),
                'origin': origin,
                'destination': destination,
                'distance': row['distance']['value'],
                'duration': row['duration']['value'],
                'duration_in_traffic': row['duration_in_traffic']['value']
            })

    return parsed_rows
