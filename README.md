This project is a simple piece of code to request Google Distance Matrix API for a study.

## Installation

[Install Python 3](https://realpython.com/installing-python/) and clone this repo.
Create a file `conf.ini` with the following format:

```
[maps]
api_key = <YOUR_GOOGLE_API_KEY>

[db]
name = db.sqlite
```

## Usage

Run the main script to request the distance matrix and store the result in `db.sqlite`:

```
$ python3 -m main.py
```

Check out the result in the database:

```
$ sqlite3 db.sqlite
> SELECT * FROM commuting_times;
```
