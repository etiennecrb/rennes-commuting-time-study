from configparser import ConfigParser
import datetime

import pytz
import requests

from db import connect, save_raw_response, save_commuting_times
from utils import get_distance_matrix, parse_response

if __name__ == '__main__':
    tz = pytz.timezone('Europe/Paris')

    config = ConfigParser()
    config.read('./conf.ini')
    api_key = config['maps']['api_key']
    db_name = config['db']['name']

    origins = [
        "22 boulevard de la Tour d'Auvergne 35000 Rennes",
        "120 rue de Fougères 35700 Rennes",
        "2 rue de Suisse 35200 Rennes",
        "10 route de Paris 35510 Cesson-Sévigné",
        "26 rue de Saint-Médard 35250 Saint-Aubin-d'Aubigné",
        "46 rue George Sand 35235 Thorigné-Fouillard",
        "26 rue de Brocéliande 35830 Betton",
        "100 avenue du Président François Mitterrand 35340 Liffré",
        "150 rue de Montreuil 35520 Melesse",
        "32 avenue Pierre le Treut 35410 Châteaugiron",
        "8 avenue des Druides 35760 Saint-Grégoire",
        "68 rue de la Vannerie 35690 Acigné",
        "26 rue de Rennes 35530 Brécé",
        "6 avenue d'Auvergne 35135 Chantepie",
        "6 rue de la Mairie 35230 Saint-Erblon",
        "6 rue de l'Ecole 35220 Marpiré",
        "10 avenue de Beauvais 35850 Gévezé",
        "4 rue Nationale 35140 Saint-Jean-sur-Couesnon",
        "2 boulevard Cahours 35150 Janzé",
        "8 rue Blaise Pascal 35170 Bruz",
        "8 rue des Blossiers 35650 Le Rheu",
        "8 rue de Bel Air 35890 Bourg-des-Comptes",
        "4 rue des Grippeaux 35160 Montfort-sur-Meu",
        "8 avenue de Djenné 35500 Vitré",
        "8 rue Saint-Pair 35190 Tinténiac",
        "6 rue du Verger 35440 Feins",
        "8 rue du Semnon 35470",
        "10 rue des Oliviers 35270 Combourg",
        "8 boulevard de l'Espadon 35400 Saint-Malo"
    ]
    destinations_buckets = [
        [
            "10 rue du Chêne Germain 35510 Cesson-Sévigné",
            "6 avenue de Belle Fontaine 35510 Cesson-Sévigné",
            "10 avenue des Champs Blancs 35510 Cesson-Sévigné"
        ],
        [
            "12 rue du Patis Tatelin 35700 Rennes",
            "10 allée de Beaulieu 35700 Rennes"
        ]
    ]

    for destinations in destinations_buckets:
        response = get_distance_matrix(origins, destinations, api_key)
        commuting_times = parse_response(response, tz)

        connexion = connect(db_name)
        save_raw_response(response, connexion)
        save_commuting_times(commuting_times, connexion)
        connexion.close()
