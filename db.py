import json
import sqlite3


def connect(db_name):
    # TODO: create schema if new db
    connexion = sqlite3.connect(db_name)
    c = connexion.cursor()
    try:
        c.execute('CREATE TABLE raw_responses (data text)')
        c.execute('''
            CREATE TABLE commuting_times
            (datetime text, origin text, destination text, distance real, duration real, duration_in_traffic real)
        ''')
        connexion.commit()
    except sqlite3.OperationalError:
        # Schema is up to date
        pass
    return connexion


def save_raw_response(response, connexion):
    c = connexion.cursor()
    c.execute('INSERT INTO raw_responses VALUES (?)', [json.dumps(response)])
    connexion.commit()


def save_commuting_times(commuting_times, connexion):
    c = connexion.cursor()
    c.executemany(
        '''
            INSERT INTO commuting_times (datetime, origin, destination, distance, duration, duration_in_traffic)
            VALUES (?,?,?,?,?,?)
        ''',
        [(d['datetime'], d['origin'], d['destination'], d['distance'], d['duration'], d['duration_in_traffic'])
         for d in commuting_times]
    )
    connexion.commit()
